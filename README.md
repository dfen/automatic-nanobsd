# automatic nanobsd

Builds bhyve VM for transmission-daemon

## Usage

From VM host:

	ssh -A user@BUILDMACHINE "sudo ~/automatic-nanobsd/rebuild"
	scp -A user@BUILDMACHINE:/tmp/bhyve.img.new.tgz /var/db
	bhyvectl --destroy --vm=VMNAME

## mkjail

Work in progress: on a FreeBSD host system, it's redundant to run another
kernel within bhyve when the jail system is available. The mkjail/deploy-jail
scripts are intended to build minimal userspace images to serve the same
purpose as the nanobsd VMs

## references
- [nanobsd(8)](https://www.freebsd.org/cgi/man.cgi?query=nanobsd&manpath=FreeBSD+12.2-RELEASE+and+Ports)
- [Updating FreeBSD from Source](https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/makeworld.html)
- [config(5) kernel config file](https://www.freebsd.org/cgi/man.cgi?query=config&sektion=5&manpath=FreeBSD+12.2-RELEASE+and+Ports)
